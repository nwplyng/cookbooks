# -*- mode: ruby -*-
# vi: set ft=ruby :
require './chef-json.rb'

Vagrant.configure("2") do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  config.vm.hostname = "vagrant-berkself-aws"

  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = "vagrant-debian-aws"

  # The url from where the 'config.vm.box' box will be fetched if it
  # doesn't already exist on the user's system.
  config.vm.box_url = "/Volumes/gStore/work/VirtualBox VMs/base-ubuntu-chef-aws/package.box"

  # Assign this VM to a host-only network IP, allowing you to access it
  # via the IP. Host-only networks can talk to the host machine as well as
  # any other machines on the same network, but cannot be accessed (through this
  # network interface) by any external networks.
  config.vm.network :private_network, ip: "192.168.33.1"
  # config.vm.network :hostonly, "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.

  # config.vm.network :public_network

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  config.vm.network :forwarded_port, guest: 80, host: 8080
  config.vm.network :forwarded_port, guest: 9200, host: 9200
  # config.vm.network :forwarded_port, guest: 3306, host: 3306

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider :virtualbox do |vb|
  #   # Don't boot with headless mode
  #   vb.gui = true
  #
  #   # Use VBoxManage to customize the VM. For example to change memory:
  #   vb.customize ["modifyvm", :id, "--memory", "1024"]
  # end
  #
  # View the documentation for the provider you're using for more
  # information on available options.

  config.ssh.max_tries = 40
  config.ssh.timeout   = 120

  # The path to the Berksfile to use with Vagrant Berkshelf
  # config.berkshelf.berksfile_path = "./Berksfile"

  # Enabling the Berkshelf plugin. To enable this globally, add this configuration
  # option to your ~/.vagrant.d/Vagrantfile file
  config.berkshelf.enabled = false

  # An array of symbols representing groups of cookbook described in the Vagrantfile
  # to exclusively install and copy to Vagrant's shelf.
  # config.berkshelf.only = []

  # An array of symbols representing groups of cookbook described in the Vagrantfile
  # to skip installing and copying to Vagrant's shelf.
  # config.berkshelf.except = []

  # Setup run
  config.ssh.username = "vagrant"

  # config.vm.provision :shell, :inline => "export PATH=/opt/chef/bin:$PATH;echo $PATH"

  config.vm.provision :chef_solo do |chef|
    chef.cookbooks_path = ["../opsworks-cookbooks/", "."]
    # this provision block upgrades the Chef Client before the real
    # Chef run starts
        # chef.log_level = :debug
    chef.json = aws_setup_chef_config.merge(custom_json)
    chef.run_list = [
      "recipe[nw_app_server::club_vagrant_recipes]",
      "recipe[opsworks_initial_setup]",
      "recipe[ssh_host_keys]",
      "recipe[ssh_users]",
      "recipe[mysql::client]",
      "recipe[ruby_custom::default]",  # any ruby install will work as long as there is a [:ruby][:version] ruby at /user/local/bin/ruby
      "recipe[opsworks_bundler]",
      "recipe[dependencies]",
      "recipe[nw_app_server::packages]",
      # "recipe[ebs]",
      # "recipe[opsworks_ganglia::client]",
      "recipe[unicorn::rails]",
      "recipe[resque::setup]",
      "recipe[nw_app_server::setup_env]",
      "recipe[resque::config]",
      # "recipe[opsworks_ganglia::configure-client]",
      # "recipe[agent_version]",
      "recipe[nw_app_server::setup_configs]",
      "recipe[rails::configure]",
      'recipe[elasticsearch::install]',
      "recipe[redis]"
    ]
  end if true

  # Deploy run
  config.vm.provision :chef_solo do |chef|
    chef.cookbooks_path = ["../opsworks-cookbooks/", '.']
    chef.log_level = :debug

    chef.json = aws_deploy_chef_config.merge(custom_json)

    chef.run_list = [
      "recipe[nw_app_server::club_vagrant_recipes]",
      "recipe[nw_app_server::setup_nwpfm]",
      'recipe[elasticsearch]',
      "recipe[deploy::default]",
      "recipe[deploy::rails]",
      "recipe[resque::default]",
      "recipe[deploy::rails-restart]"
    ]
  end if true
end
