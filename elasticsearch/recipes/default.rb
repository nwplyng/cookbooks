#-*- encoding : utf-8 -*-

# Create Directories
[ node.elasticsearch[:path][:conf], node.elasticsearch[:path][:data], node.elasticsearch[:path][:logs], node.elasticsearch[:path][:pids] ].each do |path|
  directory path do
    owner node.elasticsearch[:user] and group node.elasticsearch[:group] and mode 0755
    recursive true
    action :create
  end
end

# Configration
template "elasticsearch.yml" do
  path   "#{node.elasticsearch[:path][:conf]}/elasticsearch.yml"
  source "elasticsearch.yml.erb"
  owner node.elasticsearch[:user] and group node.elasticsearch[:group] and mode 0755

  notifies :restart, 'service[elasticsearch]'
end

template "elasticsearch-env.sh" do
  path   "#{node.elasticsearch[:path][:conf]}/elasticsearch-env.sh"
  source "elasticsearch-env.sh.erb"
  owner node.elasticsearch[:user] and group node.elasticsearch[:group] and mode 0755
end


# Init File
template "elasticsearch.init" do
  path   "/etc/init.d/elasticsearch"
  source "elasticsearch.init.erb"
  owner 'root' and mode 0755
end

# Auto start
service "elasticsearch" do
  supports :status => true, :restart => true
  action [ :start ]
end

package 'monit' do
  action :install
end

# Init File
template "monitrc" do
  path   "/etc/monit/monitrc"
  source "monitrc.erb"
  owner 'root' and mode 0600
end

service "monit" do
  supports :status => true, :restart => true
  action [ :start ]
end

# Monitoring by Monit
template "elasticsearch.monitrc" do
  path   "/etc/monit/conf.d/elasticsearch.monitrc"
  source "elasticsearch.monitrc.erb"
  owner 'root' and mode 0755
end
