def custom_json
	{
    "cust_env" => {
      'AIRBREAK_HOST' => '127.0.0.1',
      'AIRBREAK_KEY' => 'key',
      'AIRBREAK_PORT' => '3001',
      'AWS_ACCESS_KEY_ID' => 'foobar',
      'AWS_SECRET_ACCESS_KEY' => 'bar',
      'ELASTICSEARCH_URI' => '127.0.0.1',
      'MIXPANEL_API_KEY' => 'lkdf',
      'MIXPANEL_TOKEN' => 'lkdf',
      'NEWRELIC_KEY' => 'newrelic',
      'REDIS_URI' => '127.0.0.1'
    },
		"sudoers" => [{'name'=>'vagrant'}],
		"nw"=>{
				"ses"=>{
					"access_key_id"=>'key', "secret_access_key"=> 'foobar'
				},
				"newrelic"=>{"key"=>'new_relic_ke'},
				"app"=>{"environment"=>'production'},
				"redis"=>{'port'=>6379,'host'=>'192.168.1.45'},
				'api_key'=>'api_key'
		}
	}
end

def aws_deploy
  {
    "nwplyng" => {
      "deploy_to" => "/srv/www/nwplyng",
      "application" => "nwplyng",
      "domains" => ["nwplyng.com", "nwplyng"],
      "application_type" => "rails",
      "mounted_at" => nil,
      "rails_env" => "production",
      "ssl_support" => false,
      "ssl_certificate" => "-----BEGIN CERTIFICATE----END CERTIFICATE-----",
      "ssl_certificate_key" => "-----BEGIN PRIVATE KEY----END PRIVATE KEY-----",
      "ssl_certificate_ca" => nil,
      "document_root" => "public",
      "restart_command" => nil,
      "sleep_before_restart" => 0,
      "symlink_before_migrate" => {
        "config/database.yml" => "config/database.yml",
        "config/memcached.yml" => "config/memcached.yml",
				'config/newrelic.yml'=>'config/newrelic.yml',
				'config/resque.yml'=>'config/resque.yml',
				'config/thin.yml'=>'config/thin.yml',
				'config/check_in_response.yml'=>'config/check_in_response.yml',
				'config/amses.yml'=>'config/amses.yml',
				'config/api_key.yml'=>'config/api_key.yml',
				'config/admin.yml'=>'config/admin.yml'
      },
      "symlinks" => {
				'public/artist_images'=>'public/artist_images', # additions
				'public/user_uploads'=>'public/user_uploads',
				'public/badges'=>'public/badges',
        "system" => "public/system",
        "pids" => "tmp/pids",
        "log" => "log"
      },
      "database" => {  # changed
        "host" => '192.168.1.45',
        "adapter" => 'mysql2',
        "database" => "nwplaying_backend_test",
        "username" => "goutham",
        "password" => 'goutham',
        "reconnect" => true
      },
      "memcached" => {
        "host" => nil,
        "port" => 11211
      },
      "migrate" => false, # changed
      "auto_bundle_on_deploy" => true,
      "precompile_assets"=> false,
      "scm" => {
        "scm_type" => "git",
        "repository" => "gouthamvel@192.168.1.45:/Users/gouthamvel/Development/nwplyng/local_repo",
        "revision" => "master",
        "ssh_key" => File.open("/Users/gouthamvel/.ssh/id_rsa").read,
        "user" => nil,
        "password" => nil,
        "shallow_clone"=> true
      }
    }
  }
end

def aws_setup_chef_config

  {
    "dependencies" => {
      "gem_binary" => "/usr/bin/gem", # hack
      "gems" => {},
      "debs" => {}
    },
    "ebs" => {
      "devices" => {},
      "raids" => {}
    },
    "ssh_users" => {},
    "opsworks_rubygems" => {
      "version" => "1.8.24"
    },
    "opsworks_bundler" => {
      "version" => "1.3.4",
      "manage_package" => true
    },
    "ssh_host_rsa_key_private" =>  File.open("/Users/gouthamvel/.vagrant.d/insecure_private_key").read,
    "ssh_host_rsa_key_public" => File.open("/Users/gouthamvel/.vagrant.d/insecure_public_key").read,
    "ssh_host_dsa_key_private" => "",
    "ssh_host_dsa_key_public" => "",
    "languages" => {
      "ruby" => {
        "ruby_bin" => "/usr/bin/ruby",
        'default_version'=>'1.9.1'
      }
    },
    "rails" => {
      "max_pool_size" => 5
    },
    "unicorn" => {},
    "deploy" => aws_deploy,
    "opsworks" => {
      "activity" => "setup",
      "valid_client_activities" => ["reboot",
        "stop",
        "setup",
        "configure",
        "update_dependencies",
        "install_dependencies",
        "update_custom_cookbooks",
        "execute_recipes"
      ],
      "sent_at" => 1371472187,
      "deployment" => nil,
      "layers" => {
        "rails-app" => {
          "name" => "Rails Application Server",
          "id" => "b670cfdc-4e09-4835-be67-4b1a35312f6d",
          "instances" => {}
        },
        "es" => {
          "name" => "Elastic search",
          "id" => "94bfbdb5-6bd0-44f1-9770-b73c32b9aae9",
          "instances" => {}
        }
      },
      "applications" => [{
        "name" => "nwplyng",
        "slug_name" => "nwplyng",
        "application_type" => "rails"
      }],
      "stack" => {
        "name" => "Nwplyng"
      },
      "instance" => {
        "id" => "4fd92f93-32ca-484d-9326-c33e0612e731",
        "hostname" => "rails-app1",
        "instance_type" => "m1.small",
        "public_dns_name" => "ec2-54-218-248-106.us-west-2.compute.amazonaws.com",
        "private_dns_name" => "ip-10-251-10-206.us-west-2.compute.internal",
        "ip" => "54.218.248.106",
        "private_ip" => "10.251.10.206",
        "architecture" => "x86_64",
        "layers" => ["rails-app"],
        "backends" => 5,
        "aws_instance_id" => "i-ecebb1d9",
        "region" => "us-west-2",
        "availability_zone" => "us-west-2a"
      },
      "ruby_version" => "1.9.3",
      "ruby_stack" => "mysql::client", # hack to ignore aws ruby install
      "rails_stack" => {
        "name" => "nginx_unicorn"
      }
    },
    "opsworks_custom_cookbooks" => {
      "enabled" => true,
      "enable_submodules"=> true,
      "scm" => {
        "type" => "git",
        "repository" => "git@bitbucket.org:nwplyng/cookbooks.git",
        "user" => nil,
        "password" => nil,
        "revision" => nil,
        "ssh_key" => File.open("/Users/gouthamvel/.ssh/bitbucket_aws").read
      },
      "recipes" => ["opsworks_initial_setup",
        "ssh_host_keys",
        "ssh_users",
        "mysql::client",
        "dependencies",
        "ebs",
        "opsworks_ganglia::client",
        "opsworks_stack_state_sync",
        "unicorn::rails",
        "deploy::default",
        "deploy::rails",
        "test_suite",
        "opsworks_cleanup"
      ]
    }
  }
end

def aws_deploy_chef_config
	aws_setup_chef_config.merge(
	  {
	    "opsworks" => {
	      "agent_version" => "123",
	      "activity" => "configure",
	      "valid_client_activities" => ["reboot",
	        "stop",
	        "setup",
	        "configure",
	        "update_dependencies",
	        "install_dependencies",
	        "update_custom_cookbooks",
	        "execute_recipes"
	      ],
	      "sent_at" => 1371473114,
	      "deployment" => nil,
	      "layers" => {
	        "rails-app" => {
	          "name" => "Rails Application Server",
	          "id" => "b670cfdc-4e09-4835-be67-4b1a35312f6d",
	          "instances" => {
	            "rails-app1" => {
	              "public_dns_name" => "ec2-54-218-248-106.us-west-2.compute.amazonaws.com",
	              "private_dns_name" => "ip-10-251-10-206.us-west-2.compute.internal",
	              "backends" => 5,
	              "ip" => "54.218.248.106",
	              "private_ip" => "10.251.10.206",
	              "instance_type" => "m1.small",
	              "status" => "online",
	              "aws_instance_id" => "i-ecebb1d9",
	              "elastic_ip" => nil,
	              "created_at" => "2013-06-17T02:02:32+00:00",
	              "booted_at" => "2013-06-17T12:29:45+00:00",
	              "region" => "us-west-2",
	              "availability_zone" => "us-west-2a"
	            }
	          }
	        },
	        "es" => {
	          "name" => "Elastic search",
	          "id" => "94bfbdb5-6bd0-44f1-9770-b73c32b9aae9",
	          "instances" => {}
	        }
	      },
	      "applications" => [{
	        "name" => "nwplyng",
	        "slug_name" => "nwplyng",
	        "application_type" => "rails"
	      }],
	      "stack" => {
	        "name" => "Nwplyng"
	      },
	      "instance" => {
	        "id" => "4fd92f93-32ca-484d-9326-c33e0612e731",
	        "hostname" => "rails-app1",
	        "instance_type" => "m1.small",
	        "public_dns_name" => "ec2-54-218-248-106.us-west-2.compute.amazonaws.com",
	        "private_dns_name" => "ip-10-251-10-206.us-west-2.compute.internal",
	        "ip" => "54.218.248.106",
	        "private_ip" => "10.251.10.206",
	        "architecture" => "x86_64",
	        "layers" => ["rails-app"],
	        "backends" => 5,
	        "aws_instance_id" => "i-ecebb1d9",
	        "region" => "us-west-2",
	        "availability_zone" => "us-west-2a"
	      },
	      "ruby_version" => "1.9.3",
	      "ruby_stack" => "mysql::client", # hack to ignore aws ruby install
	      "rails_stack" => {
	        "name" => "nginx_unicorn"
	      }
	    },
	    "opsworks_custom_cookbooks" => {
	      "enabled" => true,
	      "scm" => {
	        "type" => "git",
	        "repository" => "git@bitbucket.org:nwplyng/cookbooks.git",
	        "user" => nil,
	        "password" => nil,
	        "revision" => nil,
	        "ssh_key" => File.open("/Users/gouthamvel/.ssh/bitbucket_aws").read
	      },
	      "recipes" => ["opsworks_ganglia::configure-client",
	        "ssh_users",
	        "agent_version",
	        "opsworks_stack_state_sync",
	        "rails::configure",
	        "test_suite",
	        "opsworks_cleanup"
	      ]
	    }
	  })
end
