node[:deploy].each do |application, deploy|

	(node[:cust_env] || {}).each do |key, val|
		bash "Adding #{key} env to bashrc" do
		    user deploy[:user]
		    code <<-EOT
			    sed -i.old "/export #{key.upcase}=.*/d" /home/#{deploy[:user]}/.bashrc
		      echo "export #{key.upcase}='#{val}'" >> /home/#{deploy[:user]}/.bashrc
		    EOT
		end
	end

end

