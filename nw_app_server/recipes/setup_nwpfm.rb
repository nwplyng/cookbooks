include_recipe "nginx::service"


template "#{node[:nginx][:dir]}/sites-available/nwp.fm" do
    Chef::Log.debug("Generating Nginx site template for nwp.fm")
    source "nwp.fm.erb"
    owner "root"
    group "root"
    mode 0644
    if File.exists?("#{node[:nginx][:dir]}/sites-enabled/nwp.fm")
      notifies :reload, resources(:service => "nginx"), :delayed
    end
end

execute "nxensite nwp.fm" do
  command "/usr/sbin/nxensite nwp.fm"
  notifies :reload, resources(:service => "nginx")
  not_if do File.symlink?("#{node[:nginx][:dir]}/sites-enabled/nwp.fm") end
end
