node[:deploy].each do |application, deploy|

	app_root_path = node[:deploy][application][:current_path]
	rails_env = node[:deploy][application][:rails_env]

	if system("cd #{app_root_path} && bundle exec rake -T | grep assets:precompile 1>/dev/null")
		Chef::Log.info("Assets detected. Running assets precompile.")
		Chef::Log.info("sudo su deploy -c 'cd #{app_root_path} && RAILS_ENV=#{rails_env} bundle exec rake assets:precompile'")
		Chef::Log.info(`sudo su deploy -c 'cd #{app_root_path} && RAILS_ENV=#{rails_env} bundle exec rake assets:precompile 2>&1'`)
	end
end
