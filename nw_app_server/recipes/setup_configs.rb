
node[:deploy].each do |application, deploy|

  opsworks_deploy_user do
    deploy_data deploy
  end


  opsworks_deploy_dir do
    user deploy[:user]
    group deploy[:group]
    path deploy[:deploy_to]
  end

  template "#{deploy[:deploy_to]}/shared/config/amses.yml" do
    source "amses.yml.erb"
    cookbook cookbook_name.to_s
    owner deploy[:user]
    group deploy[:group]
    mode "644"
    variables( :ses =>{
    	:access_key_id => node[:nw][:ses][:access_key_id],
    	:secret_access_key => node[:nw][:ses][:secret_access_key]
    })
  end

  template "#{deploy[:deploy_to]}/shared/config/newrelic.yml" do
    source "newrelic.yml.erb"
    cookbook cookbook_name.to_s
    owner deploy[:user]
    group deploy[:group]
    mode "644"
    variables(
    	:key => node[:nw][:newrelic][:key]
    )
  end


  template "#{deploy[:deploy_to]}/shared/config/resque.yml" do
    source "resque.yml.erb"
    cookbook cookbook_name.to_s
    owner deploy[:user]
    group deploy[:group]
    mode "644"
    variables(
  		:rails_env=> node[:nw][:app][:environment],
  		:port =>  node[:nw][:redis][:port],
  		:host => node[:nw][:redis][:host]
    )
  end

  template "#{deploy[:deploy_to]}/shared/config/api_key.yml" do
    source "api_key.yml.erb"
    cookbook cookbook_name.to_s
    owner deploy[:user]
    group deploy[:group]
    mode "644"
    variables(
  		:api_key => node[:nw][:api_key]
    )
  end
end
