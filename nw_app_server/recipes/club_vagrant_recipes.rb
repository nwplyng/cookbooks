path = "/tmp/vagrant-chef-1"

cookbook_repos = Dir.glob(File.join(path, 'chef-solo-*')).map { |e| File.basename e }.sort

master_repo = cookbook_repos.shift

cookbook_repos.each do |dir|
	execute "update master cookbook with #{dir}" do
		command "cp -rf #{File.join(path, dir,'cookbooks','/*')} #{File.join(path, master_repo, 'cookbooks/')}"
	end

end
