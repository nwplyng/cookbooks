# setup rails app first

node[:deploy].each do |application, deploy|

	node[:opsworks][:rails_stack][:restart_command] = 'echo "Skipping unicorn restart -- this is resque"'
	deploy[:migrate] = false
	deploy[:precompile_assets] = false


	# Monitoring by Monit
	template "resque.monitrc" do
	  path   "/etc/monit/conf.d/resque.monitrc"
	  source "resque.monitrc.erb"
	  owner 'root' and mode 0755
	  variables({
	  	:deploy_to=> deploy[:deploy_to] ,
	  	:queues=> '*',
	  	:user=> deploy[:user],
	  	:group=> deploy[:group],
	  	:env => deploy[:environment]
	  	})
	end

		# Monitoring by Monit
	template "aux_push_notify.monitrc" do
	  path   "/etc/monit/conf.d/aux_push_notify.monitrc"
	  source "aux_push_notify.monitrc.erb"
	  owner 'root' and mode 0755
	  variables({
	  	:deploy_to=> deploy[:deploy_to] ,
	  	:queues=> '*',
	  	:user=> deploy[:user],
	  	:group=> deploy[:group],
	  	:env=> deploy[:environment]
	  	})
	end

	opsworks_deploy do
	  deploy_data deploy
	  app application
	end
end
service "monit" do
  supports :status => true, :restart => true
  action [ :start , :restart]
end
