package 'monit' do
  action :install
end

node[:deploy].each do |application, deploy|
	opsworks_rails do
	  deploy_data deploy
	  app application
	end

	node[:opsworks][:rails_stack][:restart_command] = 'echo "Skipping unicorn restart -- this is resque"'
	deploy[:migrate] = false
	deploy[:precompile_assets] = false

	opsworks_deploy do
	  deploy_data deploy
	  app application
	end

	# # Init File
	# template "cron.sh" do
	#   path   "#{deploy[:deploy_to]}/shared/cron.sh"
	#   source "cron.sh.erb"
	#   owner deploy[:user]
	#   group deploy[:group]
	#   mode 0711
	#   variables({
	#   	:user=> deploy[:user],
	#   	:deploy_to => deploy[:deploy_to]
	#   	})
	# end

	# cron "send out mailing" do
	# 	user deploy[:user]
	#   minute "*/2"
	#   command "cd #{deploy[:deploy_to]} && ./shared/cron.sh"
	# end

	# Init File
	template "monitrc" do
	  path   "/etc/monit/monitrc"
	  source "monitrc.erb"
	  owner 'root' and mode 0600
	end

end
